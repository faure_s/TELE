#include <math.h>
#include <stdlib.h>
#include "kmeans.h"

int cmp_unsignedchar(const void *a, const void *b)
{
  return (*(unsigned char*)a - *(unsigned char*)b);
}

unsigned char **init_pixels(int size)
{
  unsigned char **array = malloc(size * sizeof (unsigned char*));
  for (int i = 0; i < size; i++)
    array[i] = malloc(6 * sizeof (unsigned char));
  return array;
}

unsigned char **init_centers(int K)
{
  unsigned char **array = malloc(K * sizeof (unsigned char*));
  unsigned char init_val = 254;
  for (int i = 0; i < K; i++)
  {
    array[i] = malloc(6 * sizeof (unsigned char));
    for (int j = 0; j < 5; j++)
      array[i][j] = init_val;
    array[i][5] = 0;
    init_val = init_val - (unsigned char)(254 / (unsigned char)(K - 1));
  }
  return array;
}

unsigned char *makevect(guchar *img, int nbline, int nbcol, long pos, int center)
{
  unsigned char *vect = malloc(6 * sizeof (unsigned char));
  vect[1] = *(img + (pos * 3) + 1);

  if (pos < (nbcol * 3))
    vect[0] = vect[1];
  else
    vect[0] = *(img + ((pos  - nbcol) * 3) + 1);

  if (pos >= (nbcol * nbline * 3) - (nbcol * 3))
    vect[3] = vect[1];
  else
    vect[3] = *(img + ((pos + nbcol) * 3) + 1);

  if (pos % nbcol == 0)
    vect[4] = vect[1];
  else
    vect[4] = *(img + (pos * 3) - 3 + 1);

  if ((pos + 1) % nbcol == 0)
    vect[2] = vect[1];
  else
    vect[2] = *(img + (pos * 3) + 3 + 1);

  vect[5] = center;
  return vect;
}

void fill_pixels(unsigned char **array, guchar *img, int nbline, int nbcol, int K)
{
  int totalpixel = nbline * nbcol;
  for (int i = 0; i < totalpixel; i++)
  {
    unsigned char *vs = makevect(img, nbline, nbcol, i, 0);
    qsort(vs, 5, sizeof (unsigned char), cmp_unsignedchar);
    array[i] = vs;
  }
}

double dist(unsigned char *v, unsigned char *center)
{
  double tmp = 0;
  for (int i = 0; i < 5 ; i++)
    tmp += (v[i] - center[i]) * (v[i] - center[i]);
  return sqrt(tmp);
}

int update_pixels(unsigned char **pixels, int size, unsigned char **centers, int K)
{
  int **sums = malloc(K * sizeof (int *));
  for (int i = 0; i < K; i++)
    sums[i] = malloc(6 * sizeof(int));
  int nb_changed = 0;
  for (int i = 0; i < size; i++)
  {
    double closest_dist = dist(pixels[i], centers[0]);
    int closest_index = 0;
    for (int j = 1; j < K; j++)
    {
      double cur_dist = dist(pixels[i], centers[j]);
      if (cur_dist < closest_dist)
      {
        closest_dist = cur_dist;
        closest_index = j;
      }
    }
    if (closest_index != pixels[i][5])
      nb_changed++;
    pixels[i][5] = closest_index;
    for (int j = 0; j < 5; j++)
      sums[closest_index][j] += pixels[i][j];
    sums[closest_index][5]++;
  }
  for (int i = 0; i < K; i++)
  {
    int nb = sums[i][5];
    if (nb != 0)
    {
      for (int j = 0; j < 5; j++)
        centers[i][j] = sums[i][j] / nb;
    }
  }
  if (!nb_changed)
  {
    for (int i = 0; i < K; i++)
      free(sums[i]);
    free(sums);
  }
  else
  {
    for (int i = 0; i < K; i++)
      for (int j = 0; j < 6; j++)
        sums[i][j] = 0;
  }
  return nb_changed;
}
