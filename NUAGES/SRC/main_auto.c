#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include "kmeans.h"

void ComputeImageAuto(guchar *pucImaOrig, int NbLine, int NbCol)
{
  int iNbPixelsTotal = NbCol * NbLine;

  int K = 8;
  int ispanchro = 1;
  for (long i = 0; ispanchro && (i < iNbPixelsTotal); i++)
  {
    if (ispanchro)
      ispanchro = (*(pucImaOrig + (i * 3)) == *(pucImaOrig + (i * 3) + 1)) 
        && (*(pucImaOrig + (i * 3)) == *(pucImaOrig + (i * 3) + 2));
  }
  if (ispanchro)
    K = 9;

  unsigned char **pixels = init_pixels(iNbPixelsTotal);
  unsigned char **centers = init_centers(K);
  fill_pixels(pixels, pucImaOrig, NbLine, NbCol, K);

  int changes = 0;
  int iter = 0;
  do {
    iter++;
    changes = update_pixels(pixels, iNbPixelsTotal, centers, K);
  } while (changes > 0);

  int clouds_pixels = 0;
  for (int i = 0; i < iNbPixelsTotal; i++)
  {
    if (pixels[i][5] == 0)
      clouds_pixels++;
  }

  printf("Clouds percentage: %04lf%%\n",
      ((double)clouds_pixels * 100 / (double)iNbPixelsTotal));
}

int main (int argc, char **argv)
{
  gtk_init (&argc, &argv);

  DIR *d = opendir(".");
  struct dirent *dir; 
  if (d) {
    while ((dir = readdir(d)) != NULL) {
      GError *error = NULL;
      GdkPixbuf *gdkPixbufImaOrig = gdk_pixbuf_new_from_file (dir->d_name, &error);
      if (!gdkPixbufImaOrig)
        continue;
      printf("%s\n\t", dir->d_name);
      guchar *pucImaOrig = gdk_pixbuf_get_pixels(gdkPixbufImaOrig);
      int NbCol = gdk_pixbuf_get_width(gdkPixbufImaOrig); 
      int NbLine = gdk_pixbuf_get_height(gdkPixbufImaOrig);
      ComputeImageAuto(pucImaOrig, NbLine, NbCol);
    }
    closedir(d);
  }

  return 0;
}
