#include <stdio.h>
#include <gtk/gtk.h>
#include "kmeans.h"

/*******************************************************
  IL EST FORMELLEMENT INTERDIT DE CHANGER LE PROTOTYPE
  DES FONCTIONS
 *******************************************************/


/*---------------------------------------
Proto: 


But: 

Entrees: 
--->le tableau des valeurs des pixels de l'image d'origine
(les lignes sont mises les unes � la suite des autres)
--->le nombre de lignes de l'image,
--->le nombre de colonnes de l'image,
--->le tableau des valeurs des pixels de l'image resultat
(les lignes sont mises les unes � la suite des autres)


Sortie:

Rem: 

Voir aussi:

---------------------------------------*/
void ComputeImage(guchar *pucImaOrig, 
    int NbLine,
    int NbCol, 
    guchar *pucImaRes)
{
  int iNbPixelsTotal, iNumPix;
  int iNumChannel, iNbChannels = 3; /* on travaille sur des images couleurs*/

  iNbPixelsTotal = NbCol * NbLine;

  int K = 8;
  int ispanchro = 1;
  for (long i = 0; ispanchro && (i < iNbPixelsTotal); i++)
  {
    if (ispanchro)
      ispanchro = (*(pucImaOrig + (i * 3)) == *(pucImaOrig + (i * 3) + 1)) 
        && (*(pucImaOrig + (i * 3)) == *(pucImaOrig + (i * 3) + 2));
  }
  if (ispanchro)
    K = 9;

  unsigned char **pixels = init_pixels(iNbPixelsTotal);
  unsigned char **centers = init_centers(K);
  fill_pixels(pixels, pucImaOrig, NbLine, NbCol, K);

  int changes = 0;
  do {
    changes = update_pixels(pixels, iNbPixelsTotal, centers, K);
  } while (changes > 0);


  int clouds_pixels = 0;
  for (iNumPix = 0; iNumPix < iNbPixelsTotal * iNbChannels;
      iNumPix = iNumPix + iNbChannels){
    if (pixels[iNumPix / iNbChannels][5] == 0)
    {
      clouds_pixels++;
      *(pucImaRes + iNumPix + iNbChannels) = 255;
      *(pucImaRes + iNumPix + iNbChannels + 1) = 0;
      *(pucImaRes + iNumPix + iNbChannels + 2) = 255;
    }
    else
      for (iNumChannel = 0; iNumChannel < iNbChannels; iNumChannel++)
        *(pucImaRes + iNumPix + iNumChannel) = (*(pucImaOrig + iNumPix)
            + *(pucImaOrig + iNumPix + 1) + *(pucImaOrig + iNumPix + 2)) / 3;
  }

  printf("Clouds percentage: %04lf%%\n",
      ((double)clouds_pixels * 100 / (double)iNbPixelsTotal));
}

