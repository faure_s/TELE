#ifndef KMEANS_H
#define KMEANS_H

#include<gtk/gtk.h>
unsigned char **init_pixels(int size);
unsigned char **init_centers(int K);
void fill_pixels(unsigned char **array, guchar *img, int nbline, int nbcol, int K);
int update_pixels(unsigned char **pixels, int size, unsigned char **centers, int K);

#endif
